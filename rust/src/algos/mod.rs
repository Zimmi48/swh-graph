// Copyright (C) 2024  The Software Heritage developers
// See the AUTHORS file at the top-level directory of this distribution
// License: GNU General Public License version 3, or any later version
// See top-level LICENSE file for more information

//! Helpful algorithms to work on the graph

mod root_directory;
pub use root_directory::get_root_directory_from_revision_or_release;
